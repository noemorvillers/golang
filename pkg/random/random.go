package random

import (
	"math/rand"
	"time"
)

func GenerateRandomInt(min int, max int) int {
	defineSeed()
	return rand.Intn(max-min) + min
}

func GenerateRandomFloat(min float32, max float32) float32 {
	defineSeed()
	return min + rand.Float32()*(max-min)
}

func defineSeed() {
	rand.Seed(time.Now().UTC().UnixNano())
}
