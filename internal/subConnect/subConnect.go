package subConnect

import (
	"fmt"
	"os"
	"time"

	"imt-atlantique/sensorsAirport/internal/client"
	"imt-atlantique/sensorsAirport/internal/config"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

var flag bool = false

func Connect(clientId string, topic string, qosLevel byte, messageHandler func(client mqtt.Client, msg mqtt.Message)) {
	configBroker := config.LoadConfiguration("conf.json")
	mqttClient := client.Connect(configBroker.BrokerAddr+":"+configBroker.BrokerPort, clientId)
	mqttClient.Connect().Wait()

	if token := mqttClient.Subscribe(topic, qosLevel, messageHandler); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
		os.Exit(1)
	}

	for !flag {
		time.Sleep(1 * time.Second)
		//fmt.Println("waiting: ", wcount)
		//wcount += 1
	}

	tokenSub := mqttClient.Subscribe("test", 1, messageHandler)
	fmt.Println("Now waiting....")
	tokenSub.Wait()
}
