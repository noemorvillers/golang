package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type ConfigurationSensor struct {
	SensorId    string
	AirportIOTA string
	Type        string
}

func LoadConfigurationSensor(file string) ConfigurationSensor {
	return loadByFile(file)
}

func loadByFile(file string) ConfigurationSensor {
	var config ConfigurationSensor
	configFile, err := os.Open(file)
	byteValue, _ := ioutil.ReadAll(configFile)
	if err != nil {
		fmt.Println(err.Error())
	}
	defer configFile.Close()
	json.Unmarshal(byteValue, &config)
	return config
}
