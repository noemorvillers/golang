package config

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"strconv"
)

type Configuration struct {
	BrokerAddr string
	BrokerPort string
	QosLevel   byte
}

var defaultBrokerAddr = "tcp://localhost"
var defaultBrokerPort = "1884"
var defaultQosLevel = 0

func LoadConfiguration(file string) Configuration {
	var config Configuration
	if len(os.Args) > 2 {
		fmt.Println("CONFIG BY CLI")
		fmt.Println("WARNING:  Si des paramètres ne sont pas renseignés, ils prennent les valeurs pas défaut.")
		config = loadConfigurationByParams()
	} else {
		configENV, isOnePresent := loadConfigurationByEnv()
		if isOnePresent {
			fmt.Println("CONFIG BY ENV")
			config = configENV
		} else {
			fmt.Println("CONFIG BY CONF FILE")
			fmt.Println("WARNING:  Si des paramètres ne sont pas renseignés, ils prennent les valeurs pas défaut.")
			config = loadConfigurationByFile()
		}
	}
	return config
}

func loadConfigurationByParams() Configuration {
	var config Configuration

	brokerAddr := flag.String("brokerAddr", defaultBrokerAddr, "Broker address")
	brokerPort := flag.String("brokerPort", defaultBrokerPort, "Broker port")
	qosLevel := flag.Int("qosLevel", defaultQosLevel, "Qos level")

	flag.Parse()

	fmt.Println("BrokerAdrr: ", *brokerAddr)
	fmt.Println("arg0", os.Args[0])
	fmt.Println("arg1", os.Args[1])
	fmt.Println("arg2", os.Args[2])

	config.BrokerAddr = *brokerAddr
	config.BrokerPort = *brokerPort
	config.QosLevel = byte(*qosLevel)

	return config
}

func loadConfigurationByEnv() (Configuration, bool) {
	var config Configuration = Configuration{
		BrokerAddr: defaultBrokerAddr,
		BrokerPort: defaultBrokerPort,
		QosLevel:   byte(defaultQosLevel),
	}
	var isOnePresent = false

	brokerAddr := os.Getenv("BROKER_ADDR")
	brokerPort := os.Getenv("BROKER_ADDR")
	qosLevel := os.Getenv("BROKER_ADDR")

	if brokerAddr != "" || brokerPort != "" || qosLevel != "" {
		isOnePresent = true
		if brokerAddr != "" {
			config.BrokerAddr = brokerAddr
		}
		if brokerPort != "" {
			config.BrokerPort = brokerPort
		}
		if qosLevel != "" {
			i, _ := strconv.Atoi(qosLevel)
			config.QosLevel = byte(i)
		}
	}
	return config, isOnePresent
}

func loadConfigurationByFile() Configuration {
	var config Configuration
	configFile, err := os.Open("conf.json")
	defer configFile.Close()
	if err != nil {
		fmt.Println(err.Error())
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&config)
	return config
}
