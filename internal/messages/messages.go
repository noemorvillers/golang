package messages

import (
	"encoding/json"
	"fmt"
	configSensor "imt-atlantique/sensorsAirport/internal/config"
	"imt-atlantique/sensorsAirport/internal/measures"
	"time"
)

type SensorDatas struct {
	IdSensor  string
	IdAirport string // Code "IATA" sur 3 caractères
	Type      string
	Value     float32
	Timestamp time.Time
}

func InitializeSensors() {
	measures.InitPressure()
	measures.InitTemperature()
	measures.InitWind()
}

func GetMessage(conf configSensor.ConfigurationSensor) []byte {

	var datas = SensorDatas{conf.SensorId, conf.AirportIOTA, conf.Type, 0, time.Now()}

	var value = float32(0)

	if datas.Type == "TEMPERATURE" {
		value = measures.GenerateTemperature()
	} else if datas.Type == "PRESSURE" {
		value = measures.GeneratePressure()
	} else {
		value = measures.GenerateWind()
	}
	datas.Value = value

	ret, err := json.Marshal(datas)
	if err == nil {
		fmt.Println(err)
	}
	return ret
}
