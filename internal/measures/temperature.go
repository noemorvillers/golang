package measures

import (
	random "imt-atlantique/sensorsAirport/pkg/random"
)

var temp = float32(0)
var tempStep = float32(0.3)

func InitTemperature() {
	temp = random.GenerateRandomFloat(-10, 35)
}

func GenerateTemperature() float32 {
	var newTemp = random.GenerateRandomFloat(-tempStep, tempStep)
	temp = temp + newTemp
	return temp
}
