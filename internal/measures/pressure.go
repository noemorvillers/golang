package measures

import (
	random "imt-atlantique/sensorsAirport/pkg/random"
)

var pressure = float32(0)
var pressureStep = float32(0.005)

func InitPressure() {
	pressure = random.GenerateRandomFloat(1020, 1030)
}

func GeneratePressure() float32 {
	var newPressure = random.GenerateRandomFloat(-pressureStep, pressureStep)
	pressure = pressure + newPressure
	return pressure
}
