package measures

import (
	random "imt-atlantique/sensorsAirport/pkg/random"
)

var wind = float32(0)
var windStep = float32(2)

func InitWind() {
	wind = random.GenerateRandomFloat(0, 50)
}

func GenerateWind() float32 {
	var newWind = random.GenerateRandomFloat(-windStep, windStep)
	wind = wind + float32(newWind)
	return wind
}
