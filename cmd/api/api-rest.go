package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/gomodule/redigo/redis"
	"github.com/gorilla/mux"
)

type Object struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

type Average struct {
	Airport     string  `json:"airport"`
	Date        string  `json:"date"`
	AvgTemp     float64 `json:"avgTemp"`
	AvgWind     float64 `json:"avgWind"`
	AvgPressure float64 `json:"avgPressure"`
}

var conn redis.Conn

//Inspired by https://github.com/KavishShah09/Redis-API/blob/master/main.go

//on veut avoir une méthode GET data/key qui envoie une mesure avec une clé du type "CDG_TEMP_18-10-2021:08"
//et une méthode GET data?id=CDG&date=18_10_2021 qui envoie la moyenne des mesures sur une journée pour un aéroport

func main() {

	connectRedis()

	r := mux.NewRouter()

	r.HandleFunc("/data", getData).Methods("GET")
	r.HandleFunc("/data/{key}", getValue).Methods("GET")

	http.Handle("/", r)
	http.ListenAndServe(":8000", nil)

	// Ensure the connection is always closed before exiting
	defer conn.Close()

}

func connectRedis() {
	// Connect to the Redis server (default port is 6379)
	var err error
	conn, err = redis.Dial("tcp", "localhost:6379")

	if err != nil {
		log.Fatal(err)
	}
}

func getData(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var sumTemp float64 = 0
	var sumWind float64 = 0
	var sumPressure float64 = 0
	nbValueTemp := 0
	nbValueWind := 0
	nbValuePressure := 0

	airport := r.URL.Query()["id"][0]
	date := r.URL.Query()["date"][0]
	keyString := airport + "*" + date + "*"
	keys, _ := redis.Strings(conn.Do("keys", keyString))
	fmt.Println(keys)
	for _, key := range keys {
		fmt.Println(key)
		value, _ := redis.Float64(conn.Do("GET", key))
		if strings.Contains(key, "TEMP") {
			sumTemp += value
			nbValueTemp++
		} else if strings.Contains(key, "WIND") {
			sumWind += value
			nbValueWind++
		} else if strings.Contains(key, "PRESSURE") {
			sumPressure += value
			nbValuePressure++
		}
	}

	if nbValueTemp == 0 {
		nbValueTemp = 1
	}
	if nbValueWind == 0 {
		nbValueWind = 1
	}
	if nbValuePressure == 0 {
		nbValuePressure = 1
	}

	ret := Average{
		Airport:     airport,
		Date:        date,
		AvgTemp:     sumTemp / float64(nbValueTemp),
		AvgWind:     sumWind / float64(nbValueWind),
		AvgPressure: sumPressure / float64(nbValuePressure),
	}

	jsonRet, _ := json.Marshal(ret)

	fmt.Println(jsonRet)

}

func getValue(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	params := mux.Vars(r)
	fmt.Print("Getting keys from ")
	fmt.Println(params["key"])

	a, err := redis.Bytes(conn.Do("GET", params["key"]))
	if err != nil {
		log.Fatal(err)
	}

	ret := Object{
		Key:   params["key"],
		Value: string(a),
	}

	jsonRet, _ := json.Marshal(ret)

	w.Write(jsonRet)

}
