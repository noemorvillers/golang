package main

import (
	"fmt"
	"os"
	"time"

	"imt-atlantique/sensorsAirport/internal/client"
	"imt-atlantique/sensorsAirport/internal/config"
	configSensor "imt-atlantique/sensorsAirport/internal/config"
	"imt-atlantique/sensorsAirport/internal/messages"
)

func main() {

	messages.InitializeSensors()

	//Get the sensor configuration (ID, Airport, Type)
	var configSensor = configSensor.LoadConfigurationSensor(os.Args[1])
	fmt.Print("Config du sensor : ")
	fmt.Println(configSensor)

	//Get the mqtt configuration (Broker address, broker port, qos level, client id)
	var configMQTT = config.LoadConfiguration("conf.json")
	fmt.Print("Config du MQTT : ")
	fmt.Println(configMQTT)

	//Connect the sensor to the broker via mqtt
	var brokerURI = configMQTT.BrokerAddr + ":" + configMQTT.BrokerPort
	var clientId = configSensor.SensorId
	mqttClient := client.Connect(brokerURI, clientId)

	//Main loop of the sensor
	for i := 0; i < 100000; i++ {
		tokenPublish := mqttClient.Publish(getTopic(configSensor), configMQTT.QosLevel, false, messages.GetMessage(configSensor))
		tokenPublish.Wait()
		time.Sleep(time.Second * 10)
	}
}

func getTopic(conf configSensor.ConfigurationSensor) string {
	return "sensor/" + conf.AirportIOTA + "/" + conf.Type
}
