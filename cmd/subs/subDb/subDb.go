package main

import (
	"encoding/json"
	"fmt"
	messages "imt-atlantique/sensorsAirport/internal/messages"
	"imt-atlantique/sensorsAirport/internal/subConnect"
	"log"
	"strconv"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/gomodule/redigo/redis"
)

var aeroportValues = make(map[string][]float32)
var currentHour int = time.Now().Hour()

var Conn redis.Conn

var messageHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	var sensorDatas messages.SensorDatas
	err := json.Unmarshal(msg.Payload(), &sensorDatas)
	if err != nil {
		panic(err)
	}
	// aéroport_type_date:heure
	var newValue float32 = sensorDatas.Value
	keyMap := sensorDatas.IdAirport + "_" + sensorDatas.Type
	_, found := aeroportValues[keyMap]
	if !found {
		aeroportValues[keyMap] = append(aeroportValues[keyMap], newValue)
	}
	newHour := sensorDatas.Timestamp.Hour()
	if currentHour != newHour {
		var stringDb string
		var formattedTimestamp string = sensorDatas.Timestamp.Format("02_01_2006")
		for keyMap := range aeroportValues {
			var sum float32 = 0
			fmt.Println("aeroportValues: ", aeroportValues)
			for i := 0; i < len(aeroportValues[keyMap]); i++ {
				sum += (aeroportValues[keyMap][i])
			}
			var avgValue = (sum) / (float32)(len(aeroportValues[keyMap]))
			fmt.Println("avgValue: ", avgValue)
			stringDb = keyMap + "_" + formattedTimestamp + ":" + strconv.Itoa(currentHour)
			_, errRedis := Conn.Do("SET", stringDb, avgValue)
			if errRedis != nil {
				log.Fatal(err)
			}
		}
		currentHour = newHour
	} else {
		aeroportValues[keyMap] = append(aeroportValues[keyMap], newValue)
	}
}

func main() {
	// Connect to the Redis server (default port is 6379)
	var err error
	Conn, err = redis.Dial("tcp", "localhost:6379")

	if err != nil {
		log.Fatal(err)
	}
	// Ensure the connection is always closed before exiting
	defer Conn.Close()

	subConnect.Connect("151663", "sensor/+/+", 2, messageHandler)
}
