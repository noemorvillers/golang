package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"imt-atlantique/sensorsAirport/internal/messages"
	"imt-atlantique/sensorsAirport/internal/subConnect"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

func main() {
	subConnect.Connect("96636693", "sensor/#", 2, messageHandler)
}

var messageHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	topic := msg.Topic()
	payload := msg.Payload()
	if strings.Compare(string(payload), "\n") > 0 {

		fmt.Printf("TOPIC: %s\n", topic)
		fmt.Printf("MSG: %s\n", payload)

		var datas messages.SensorDatas
		json.Unmarshal(payload, &datas)

		idAirport := datas.IdAirport
		dataType := datas.Type

		regex := regexp.MustCompile(`[0-9]{4}(-[0-9]{2}){2}`)
		dateStr := regex.FindString(datas.Timestamp.String())

		csvName := idAirport + "-" + dateStr + "-" + dataType + ".csv"

		f, err := os.OpenFile(filepath.Join("csv/", filepath.Base(csvName)), os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)

		if err != nil {

			log.Fatalln("failed to open file", err)
		}

		w := csv.NewWriter(f)
		defer w.Flush()

		valueStr := fmt.Sprintf("%f", datas.Value)
		record := []string{datas.IdSensor, datas.IdAirport, datas.Type, valueStr, datas.Timestamp.String()}

		if err := w.Write(record); err != nil {
			log.Fatalln("error writing record to file", err)
		}
	}

}
