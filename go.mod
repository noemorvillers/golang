module imt-atlantique/sensorsAirport

go 1.17

require (
	github.com/eclipse/paho.mqtt.golang v1.3.5
	github.com/gomodule/redigo v1.8.5
	github.com/gorilla/websocket v1.4.2 // indirect
	golang.org/x/net v0.0.0-20200425230154-ff2c4b7c35a0 // indirect
)

require github.com/gorilla/mux v1.8.0
